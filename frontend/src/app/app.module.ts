import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { LightboxModule } from 'ngx-lightbox';
import { SlideshowModule } from 'ng-simple-slideshow';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InfoPageComponent } from './component/info-page/info-page.component';
import { NavbarComponent } from './component/navbar/navbar.component';
import { BoxComponent } from './component/box/box.component';
import { FooterComponent } from './component/footer/footer.component';
import { ContactComponent } from './component/contact/contact.component';
import { PricingComponent } from './component/pricing/pricing.component';
import { GalleryComponent } from './component/gallery/gallery.component';
import { OtherComponent } from './component/other/other.component';
import { WeddingComponent } from './component/wedding/wedding.component';
import { HomePageComponent } from './component/home-page/home-page.component';

@NgModule({
  declarations: [
    AppComponent,
    InfoPageComponent,
    NavbarComponent,
    BoxComponent,
    FooterComponent,
    ContactComponent,
    PricingComponent,
    GalleryComponent,
    OtherComponent,
    WeddingComponent,
    HomePageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    LightboxModule,
    SlideshowModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
