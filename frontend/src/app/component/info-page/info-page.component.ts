import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-page',
  templateUrl: './info-page.component.html',
  styleUrls: ['./info-page.component.scss']
})
export class InfoPageComponent implements OnInit {

  public selectedGallery: string = null;
  public windowWidth: number = null;

  constructor() { }

  ngOnInit() {
    this.windowWidth = window.innerWidth;
    this.getWidth(window.innerWidth);
  }

  public getWidth(width): void {

  }

}
