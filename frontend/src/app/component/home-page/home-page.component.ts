import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  public imageSources = [
  'assets/img/slideshow/1.jpg',
  'assets/img/slideshow/2.jpg',
  'assets/img/slideshow/3.jpg',
  'assets/img/slideshow/4.jpg',
  'assets/img/slideshow/5.jpg',
  'assets/img/slideshow/6.jpg',
  'assets/img/slideshow/7.jpg',
];

  constructor() { }

  ngOnInit() {
  }

}
