import { Component, OnInit } from '@angular/core';
import { Lightbox } from 'ngx-lightbox';

@Component({
  selector: 'app-wedding',
  templateUrl: './wedding.component.html',
  styleUrls: ['./wedding.component.scss']
})
export class WeddingComponent implements OnInit {

  public photoSet = [];

  constructor(private lightbox: Lightbox) { }

  ngOnInit() {
    for (let i = 1; i <= 35; i++) {
      const src = 'assets/img/wedding/' + 'wed' + i + '.jpg';
      const thumb = 'assets/img/wedding/' + 'wed' + i + '_tn' + '.jpg';
      const album = {
         src: src,
         thumb: thumb
      };
 
      this.photoSet.push(album);
    }
  }

  public open(index: number): void {
    this.lightbox.open(this.photoSet, index);
  }

  public close(): void {
    this.lightbox.close();
  }

}
