import { Component, OnInit } from '@angular/core';
import { Lightbox } from 'ngx-lightbox';

@Component({
  selector: 'app-other',
  templateUrl: './other.component.html',
  styleUrls: ['./other.component.scss']
})
export class OtherComponent implements OnInit {
  
  public photoSet = [];

  constructor(private lightbox: Lightbox) { }

  ngOnInit() {
    for (let i = 1; i <= 35; i++) {
      const src = 'assets/img/other/' + i + '.jpg';
      const thumb = 'assets/img/other/' + i + '_tn' + '.jpg';
      const album = {
         src: src,
         thumb: thumb
      };

      this.photoSet.push(album);
    }
  }

  public open(index: number): void {
    this.lightbox.open(this.photoSet, index);
  }

  public close(): void {
    this.lightbox.close();
  }

}
