import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InfoPageComponent } from './component/info-page/info-page.component';
import { ContactComponent } from './component/contact/contact.component';
import { GalleryComponent } from './component/gallery/gallery.component';
import { PricingComponent } from './component/pricing/pricing.component';
import { OtherComponent } from './component/other/other.component';
import { WeddingComponent } from './component/wedding/wedding.component';
import { HomePageComponent } from './component/home-page/home-page.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'

  },
  {
    path: 'home',
    component: HomePageComponent
  },
  {
    path: 'info',
    component: InfoPageComponent
  },
  {
    path: 'gallery',
    component: GalleryComponent
  },
  {
    path: 'pricing',
    component: PricingComponent
  },
  {
    path: 'contact',
    component: ContactComponent
  },
  {
    path: 'other',
    component: OtherComponent
  },
  {
    path: 'wedding',
    component: WeddingComponent
  },
  {
    path: '**',
    redirectTo: '/home',
    pathMatch: 'full'
  }
 ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
