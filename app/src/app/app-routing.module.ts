import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from './pages/home/home.component';
import {InfoComponent} from './pages/info/info.component';
import {ContactComponent} from './pages/contact/contact.component';
import {PricingComponent} from './pages/pricing/pricing.component';
import {GalleryComponent} from './pages/gallery/gallery.component';
import {WeddingGalleryComponent} from './components/wedding-gallery/wedding-gallery.component';
import {OtherGalleryComponent} from './components/other-gallery/other-gallery.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'info',
    component: InfoComponent
  },
  {
    path: 'kontakt',
    component: ContactComponent
  },
  {
    path: 'cenik',
    component: PricingComponent
  },
  {
    path: 'galerie',
    component: GalleryComponent
  },
  {
    path: 'svatby',
    component: WeddingGalleryComponent
  },
  {
    path: 'rodinne',
    component: OtherGalleryComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
