import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';
import { InfoComponent } from './pages/info/info.component';
import { BoxComponent } from './components/box/box.component';
import { ContactComponent } from './pages/contact/contact.component';
import { PricingComponent } from './pages/pricing/pricing.component';
import { GalleryComponent } from './pages/gallery/gallery.component';
import { WeddingGalleryComponent } from './components/wedding-gallery/wedding-gallery.component';
import { LightgalleryModule } from 'lightgallery/angular';
import { OtherGalleryComponent } from './components/other-gallery/other-gallery.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    FooterComponent,
    InfoComponent,
    BoxComponent,
    ContactComponent,
    PricingComponent,
    GalleryComponent,
    WeddingGalleryComponent,
    OtherGalleryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LightgalleryModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
