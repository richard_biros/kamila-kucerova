import { Component, OnInit } from '@angular/core';
import lgZoom from 'lightgallery/plugins/zoom';
@Component({
  selector: 'app-wedding-gallery',
  templateUrl: './wedding-gallery.component.html',
  styleUrls: ['./wedding-gallery.component.scss']
})
export class WeddingGalleryComponent implements OnInit {

  constructor() { }

  settings = {
    counter: false,
    plugins: [lgZoom],
    download: false
  };

  items: string[] = [];


ngOnInit(): void {
    for (let i = 1; i < 85; i++) {
      this.items.push(`assets/img/wedding/wed${i}.jpg`)
    }
  }

}
