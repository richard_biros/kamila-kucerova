import { Component, OnInit } from '@angular/core';
import lgZoom from 'lightgallery/plugins/zoom';

@Component({
  selector: 'app-other-gallery',
  templateUrl: './other-gallery.component.html',
  styleUrls: ['./other-gallery.component.scss']
})
export class OtherGalleryComponent implements OnInit {

  items: string[] = [];
  settings = {
    counter: false,
    plugins: [lgZoom],
    download: false
  };

  constructor() { }

  ngOnInit(): void {
    for (let i = 1; i < 57; i++) {
      this.items.push(`assets/img/other/other${i}.jpg`)
    }
  }

}
